#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2023/3/17 16:25
# @Author : sparkle_code_guy
'''
conda create -n paddlespeech python=3.7 cudnn=7.6.5 cudatoolkit=10.1.243 ffmpeg x264
pip install paddlepaddle -i https://mirror.baidu.com/pypi/simple
pip install paddlespeech -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install gradio
'''
import paddle
import gradio as gr
from paddlespeech.cli.asr import ASRExecutor

asr_executor = ASRExecutor()

def recognize_txt(audio_input,record_input,model_choose):
    if model_choose=="more faster":
        inner_model = "conformer_u2pp_online_wenetspeech"
    else:
        inner_model = "conformer_wenetspeech"
    if audio_input:
        audio_message = audio_input
    else:
        audio_message = record_input
    text = asr_executor(
        audio_file=audio_message,
        model=inner_model,
        lang='zh',
        sample_rate=16000,
        config=None,  # Set `config` and `ckpt_path` to None to use pretrained model.
        ckpt_path=None,

        force_yes=False,
        device=paddle.get_device())
    return text


record_input = gr.components.Audio(label='record',source="microphone",type='filepath')
audio_input = gr.components.Audio(label='upload',source="upload",type='filepath')
model_choose = gr.Radio(["more faster", "more accurate"],value="more faster", label="ASR Model", info="which model you want choose?")
iface = gr.Interface(title="致慧方程语音识别演示环境",fn=recognize_txt, inputs=[audio_input,record_input,model_choose], outputs="text",interpretation="default")
iface.launch(share=False,server_name='0.0.0.0',server_port=30001)
